
'use strict'

let reekoh = require('reekoh')
let _plugin = new reekoh.plugins.Stream()

let isEmpty = require('lodash.isempty')
let get = require('lodash.get')

let BPromise = require('bluebird')
let safeParse = BPromise.method(JSON.parse)

const WebSocket = require('ws')
let client

_plugin.once('ready', () => {
  let deviceKey = _plugin.config.deviceKey || 'device'

  client = new WebSocket(`ws://${_plugin.config.host}:${_plugin.config.port}`, { handshakeTimeout: 5000 })

  _plugin.log(`Websocket Stream initialized`)
  _plugin.emit('init')

  client.on('close', () => {
    _plugin.log('Disconnected from the server.')
  })

  client.onerror = (error) => {
    console.log(error.message)
    return _plugin.logException(error)
  }

  client.on('message', function incoming (data) {
    let parsedData
    let deviceId

    safeParse(data || '{}')
      .then((parsed) => {
        parsedData = parsed
        if (isEmpty(parsedData)) return BPromise.reject(new Error(`Invalid data. Data must be a valid JSON String. Raw Message: ${data}`))

        deviceId = get(parsedData, deviceKey)

        if (isEmpty(deviceId)) {
          return BPromise.reject(new Error(`Invalid data sent. Data must have a "${deviceKey}" field which corresponds to a registered Device ID.`))
        }

        return _plugin.requestDeviceInfo(deviceId)
      })
      .then((deviceInfo) => {
        if (deviceInfo) {
          return _plugin.pipe(Object.assign(parsedData, { rkhDeviceInfo: deviceInfo }))
        } else return BPromise.reject(new Error(`Device ${deviceId} not registered`))
      })
      .then(() => {
        return _plugin.log({
          title: 'Websocket Stream - Data Received',
          data: parsedData
        })
      })
      .then(() => {
        _plugin.emit('ok')
      })
      .catch((err) => {
        _plugin.logException(err)
      })
  })

  client.onopen = () => {
    _plugin.log('Connection Established.')
  }
})

module.exports = _plugin
