FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/ftp-stream

WORKDIR /home/node/ftp-stream

RUN npm install pm2@2.6.1 -g

EXPOSE 8080
CMD pm2-docker --json app.yml
