## Websocket Stream

## Description

Websocket Stream enables a Reekoh Pipeline to stream data on a Websocket Server.

## Configuration

The Websocket Stream configuration can be done once you've created your own pipeline in Reekoh. To configure the plugin, you will be asked to provide the following details:

- **Stream Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **Host** - The Websocket server host to connect to.
- **Port** - (Optional) The Websocket server port to connect to. Default: 21.
- **Device ID Property/Key** - The key or property where the device id will be pulled from. Defaults to 'device'.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/stream/websocket-stream/1.0.0/websocket-config.png)

## Pull Data

In order to pull data, you need to have a Websocket Server instance to connect to. Also, a Storage/Connector Plugin to received the data is needed. In the screen screenshot below, it uses Webhooks Connector to receive and view the data. Note: look for the documentation on how to use this plugin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/stream/websocket-stream/1.0.0/websocket-pipeline.png)

## Verify Data

The data will be ingested by the Websocket Stream plugin, which will be forwarded to all the other plugins that are connected to it in the pipline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/stream/websocket-stream/1.0.0/websocket-logs.png)

The data that was pulled from the Websocket Server to Websocket Stream plugin will be forwarded to Webhooks Connector. The output of the process should appear in RequestBin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/stream/websocket-stream/1.0.0/websocket-received.png)