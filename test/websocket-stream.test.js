/* global describe, it, after, before */
'use strict'

const async = require('async')
const amqp = require('amqplib')

let _broker = null
let _channel = null
let _conn = null
let _app = null

let Broker = require('../node_modules/reekoh/lib/broker.lib')
const BROKER = 'amqp://guest:guest@127.0.0.1'

const PORT = 2222

describe('Websocket Stream Test', () => {
  before('init', () => {
    // process.env.ACCOUNT = 'adinglasan'
    // process.env.PORT = 2222
    // process.env.CONFIG = '{"host":"52.173.85.198","port":"8080","deviceKey":"device"}'
    // process.env.OUTPUT_PIPES = 'op.websocket, op.websocket'
    // process.env.PLUGIN_ID = 'websocket.stream'
    // process.env.COMMAND_RELAYS = 'cr1, cr2'
    // process.env.LOGGERS = 'logger1, logger2'
    // process.env.EXCEPTION_LOGGERS = 'ex.logger1, ex.logger2'
    // process.env.BROKER = 'amqp://guest:guest@127.0.0.1'

    _broker = new Broker()

    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(5000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#test RPC preparation', () => {
    it('should connect to broker', (done) => {
      _broker.connect(BROKER).then(() => {
        return done() || null
      }).catch((err) => {
        done(err)
      })
    })
    // for request device info
    it('should spawn temporary RPC server(device info)', (done) => {
      _broker.createRPC('server', 'device.info.rpc').then((queue) => {
        return queue.serverConsume((msg) => {
          // console.log('teeesstt', msg)
          return new Promise((resolve, reject) => {
            async.waterfall([
              async.constant(msg.content.toString('utf8')),
              async.asyncify(JSON.parse)
            ], (err, parsed) => {
              if (err) return reject(err)
              parsed.foo = 'bar'
              resolve(JSON.stringify(parsed))
            })
          })
        })
      }).then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#Send', function () {
    it('should send the to the server', function (done) {
      this.timeout(10000)

      _app = require('../app')

      const WebSocketServer = require('ws').Server
      const wss = new WebSocketServer({ port: PORT })

      wss.on('connection', (ws) => {
        if (ws.readyState === ws.OPEN) {
          ws.send(JSON.stringify({
            device: 'Device1zxcmz.,zm0',
            msg: 'testData'
          }))
        }
      })
      _app.once('ok', done)
    })
  })
})
